package noobbot;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;//generate file name by current time

import model.BotJoin;

import com.google.gson.Gson;

public class Main {
	static DateFormat dateFormat = new SimpleDateFormat("MMddHH_mm_ss");
	//get current date time
	static Date date = new Date();
	PrintWriter txtwriter = new PrintWriter("IOData/"+dateFormat.format(date)+".txt", "UTF-8");//store the IO data for debug;//for debug
    
	public static void main(String... args) throws IOException {

	//Set output to the file
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(dateFormat.format(date)+".txt");
			BufferedOutputStream bos = new BufferedOutputStream(fos, 1024);
			PrintStream ps = new PrintStream(bos, false);
			// 重定向System.out 到该文件
			System.setOut(ps);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        //System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new BotJoin(botName, botKey));
//Close the socket
        socket.close();
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    public Main(final BufferedReader reader, final PrintWriter writer, final BotJoin join) throws IOException {
	
	this.writer = writer;
        String line = null;
        send(join);
        MsgWrapper msgFromServer;
        Util switchUtil = new Util();
        
		while((line = reader.readLine()) != null) 
		{
		//get the message
			
			//switchUtil.getIdAndTick(line);
			
			txtwriter.println("\nServer:\n"+line+"\n");
			msgFromServer = gson.fromJson(line, MsgWrapper.class);
		//use switch function to choose the message type and process
			SendMsg sendMessage = switchUtil.switchUtil(msgFromServer,line);
			if(sendMessage != null)
				send(sendMessage);
        }
		txtwriter.close();
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
		txtwriter.println("\nBot:\n"+msg.toJson()+"\n");	
        writer.flush();
    }
}



