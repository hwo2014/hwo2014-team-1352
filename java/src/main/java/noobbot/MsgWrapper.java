package noobbot;

class MsgWrapper {
    public final String msgType;
    public final Object data;
//    public final String gameId;
//    public final int gameTick;
    

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
//        this.gameId = "";
//    	this.gameTick = -1;
    }
//    MsgWrapper(final String msgType, final Object data, final String gameId, final int gameTick) {
//    	this.msgType = msgType;
//    	this.data = data;
//    	this.gameId = gameId;
//    	this.gameTick = gameTick;
//    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

