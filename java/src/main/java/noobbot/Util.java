package noobbot;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;

import model.BotJoin;
import model.BotPing;
import model.BotSwitch;
import model.BotThrottle;
import model.DataCarPosition;
import model.DataCrash;
import model.DataDNF;
import model.DataFinish;
import model.DataGameEnd;
import model.DataGameInit;
import model.DataLapFinished;
import model.DataSpawn;
import model.DataYourCar;
import model.ModelPiece;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;



public class Util {
	static DateFormat dateFormat = new SimpleDateFormat("MMddHH_mm_ss");
	//get current date time
	static Date date = new Date();
	PrintWriter parawriter;


	Gson gson = new Gson();
	public static String TEAMNAME="OkbOy";
	public String gameId="";
	public int gameTick=-1;
	public ModelPiece[] pieces;
	double throttle=0.1;
	int changetime=0;
	int mod=500;
	public SendMsg switchUtil(MsgWrapper msgFromServer, String jsonData){
	    /*
		try {
	    	parawriter= new PrintWriter("IOData/"+dateFormat.format(date)+"Motion.txt", "UTF-8");//store the IO data for debug;//for debug   
	     }
	     catch (IOException e) {
	        throw new RuntimeException(e);
	     }		
		*/
	//get the data without type
        String data = gson.toJson(msgFromServer.data);
        //String data = jsonData;
	//print the state and transfer the data to right model
        if (msgFromServer.msgType.equals("join")) {					/*join*/
        	//System.out.println("Joined:");
        	BotJoin join = gson.fromJson(data, BotJoin.class);
        	//System.out.println(join+"\n");
        	
        } else if (msgFromServer.msgType.equals("yourCar")) {		/*yourCar*/
        	//System.out.println("YourCar:");
        	DataYourCar yourCar = gson.fromJson(data, DataYourCar.class);
        	//System.out.println(yourCar+"\n");
        	
        } else if (msgFromServer.msgType.equals("gameInit")) {		/*GameInit*/
        	//System.out.println("GameInit:");
        	DataGameInit gameInit = gson.fromJson(data, DataGameInit.class);
			
        	pieces = gameInit.race.track.pieces;
        	//System.out.println(gameInit+"\n");
        	
        } else if (msgFromServer.msgType.equals("gameStart")) {		/*GameStart*/
        	//System.out.println("GameStart:");
        	
        } else if (msgFromServer.msgType.equals("carPositions")) {	/*CarPositions*/
        	//System.out.println("CarPositions:");
        	getIdAndTick(jsonData);
        //Parse Array Start
        	Type listType = new TypeToken<LinkedList<DataCarPosition>>(){}.getType();    
            LinkedList<DataCarPosition> dataCarPositions = gson.fromJson(data, listType);    
            for(DataCarPosition dataCarPosition:dataCarPositions){
            	dataCarPosition.gameId = gameId;
            	dataCarPosition.gameTick = gameTick;
            	System.out.print(dataCarPosition.gameTick+" "+
            	dataCarPosition.piecePosition.pieceIndex+" "+
            	dataCarPosition.piecePosition.inPieceDistance+" "+
            	dataCarPosition.angle+" "+		
            			dataCarPosition.piecePosition.lane.startLaneIndex+" "+
            			dataCarPosition.piecePosition.lane.endLaneIndex+";");
            	
                //System.out.println(dataCarPosition+"\n");
                
            	/*
            	BotSwitch botSwitch = whetherSwitch(dataCarPosition);
                if(botSwitch!= null)
                	return botSwitch;
                */
            }    
			BotThrottle botThrottle =new BotThrottle(throttle);
			
			if((gameTick%mod==0)&&(throttle>0.0))
				{
					throttle+=0.1;				
//					changetime++;
//					mod = mod - 10*changetime;
					//System.out.println("\n\n"+throttle+"\n");
				}
        	
			//System.out.println(botThrottle+"\n"); 
        	return  botThrottle;
        	
        } else if (msgFromServer.msgType.equals("throttle")) {		/*Throttle*/
        	//System.out.println("Throttle:");
        	BotThrottle throttle = gson.fromJson(data, BotThrottle.class);
        	//System.out.println(throttle+"\n");
        	
        } else if (msgFromServer.msgType.equals("gameEnd")) {		/*gameEnd*/
            //System.out.println("RaceEnd:");
            DataGameEnd gameEnd = gson.fromJson(data, DataGameEnd.class);
        	//System.out.println(gameEnd+"\n");
        	
        } else if (msgFromServer.msgType.equals("crash")) {		/*crash*/
        	//System.out.println("crash:");
        	getIdAndTick(jsonData);
        	DataCrash crash = gson.fromJson(data, DataCrash.class);
        	crash.gameId = gameId;
        	crash.gameTick = gameTick;
        	//System.out.println(crash+"\n");
        	
        } else if (msgFromServer.msgType.equals("spawn")) {		/*spawn*/
        	//System.out.println("spawn:");
        	getIdAndTick(jsonData);
        	DataSpawn spawn = gson.fromJson(data, DataSpawn.class);
        	spawn.gameId = gameId;
        	spawn.gameTick = gameTick;
        	//System.out.println(spawn+"\n");
        	
        } else if (msgFromServer.msgType.equals("lapFinished")) {		/*lapFinished*/
        	//System.out.println("lapFinished:");
        	getIdAndTick(jsonData);
        	DataLapFinished lapFinished = gson.fromJson(data, DataLapFinished.class);
        	lapFinished.gameId = gameId;
        	lapFinished.gameTick = gameTick;
        	//System.out.println(lapFinished+"\n");
        	
        } else if (msgFromServer.msgType.equals("dnf")) {		/*dnf*/
        	//System.out.println("dnf:");
        	getIdAndTick(jsonData);
        	DataDNF dnf = gson.fromJson(data, DataDNF.class);
        	dnf.gameId = gameId;
        	dnf.gameTick = gameTick;
        	System.out.println(dnf+"\n");
        	
        } else if (msgFromServer.msgType.equals("finish")) {		/*finish*/
        	//System.out.println("finish:");
        	getIdAndTick(jsonData);
        	DataFinish finish = gson.fromJson(data, DataFinish.class);
        	finish.gameId = gameId;
        	//System.out.println(finish+"\n");
        	
        } else if (msgFromServer.msgType.equals("switchLane")) {		/*switchLane*/
        	//System.out.println("switchLane:");
        	BotSwitch switchLane = gson.fromJson(data, BotSwitch.class);
        	//System.out.println(switchLane+"\n");
        	
        } else if (msgFromServer.msgType.equals("tournamentEnd")) {		/*tournamentEnd*/
        	//System.out.println("tournamentEnd:");
        	
        } else {													/*Ping*/
        	//System.out.println("Ping");
        	return new BotPing();
        }
        
        //parawriter.close();
        
        //final
		return new BotPing();
		
	}

	private BotSwitch whetherSwitch(DataCarPosition dataCarPosition) {
	//判断是否为我们的车子
		if(dataCarPosition.id.name.equals(TEAMNAME)){
			int pieceIndex = dataCarPosition.piecePosition.pieceIndex;
			double angle=0;
			int lane=dataCarPosition.piecePosition.lane.startLaneIndex;
		
		//判断最近一个弯道的方向
			if(pieceIndex<pieces.length)
				if(pieces[pieceIndex].switchFlag==true)
					for(int i=pieceIndex,flag=0;i<pieces.length;i++)
						if(pieces[i].angle!=angle && flag==0)
							continue;
						else if(flag==0)
							flag=1;
						else if(pieces[i].angle!=angle){
							angle = pieces[i].angle;
							break;
						}
		//根据弯道方向判断换赛道的方向			
			if(angle>0)
				if(lane==0){
					System.out.println("right");
					return new BotSwitch("Right");
				}
			if(angle<0)
				if(lane==1){
					System.out.println("left");
					return new BotSwitch("Left");
				}
		}
		return null;
	}

	public void getIdAndTick(String jsonData){    
        JsonReader reader = new JsonReader(new StringReader(jsonData));    
        try {    
                reader.beginObject();    
                while(reader.hasNext()){    
                    String tagName = reader.nextName();    
                    if(tagName.equals("gameId")){    
                        gameId = reader.nextString();    
                    }    
                    else if(tagName.equals("gameTick")){    
                        gameTick = reader.nextInt();    
                    } 
                    else  reader.skipValue();
                }    
                reader.endObject();    
                reader.close();
        } catch (IOException e) {    
            e.printStackTrace();    
        }  
        
	}
//	@Test
//	public void test1(){
//		String jsonData="{\"msgType\": \"join\", \"data\": {\"name\": \"Schumacher\",\"key\": \"UEWJBVNHDS\"},\"gameId\": \"ID454\", \"gameTick\":25}";
////		String jsonData="{\"name\":\"kevin\",\"age\":25}";
//		
//	parseIdAndTick(jsonData);
//		
//	}
}
