package model;

public class ModelLap {
	public int lap;
	public int ticks;
	public int millis;
	
	@Override
	public String toString() {
		return "LapInfo [lap=" + lap + ", ticks=" + ticks + ", millis="
				+ millis + "]";
	}
	
	
	
}
