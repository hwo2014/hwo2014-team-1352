package model;

public class ModelResultWithCarInfo {
	public DataYourCar car;
	public ModelResult result;
	
	@Override
	public String toString() {
		return "DataResult [car=" + car + ", result=" + result + "]";
	}
	
}
