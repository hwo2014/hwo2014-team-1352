package model;

import java.util.Arrays;

public class ModelRace {
	public ModelTrack track;
	public ModelCar[] cars;
	public ModelRaceSession raceSession;
	@Override
	public String toString() {
		return "ModelRace [track=" + track + ", cars=" + Arrays.toString(cars)
				+ ", raceSession=" + raceSession + "]";
	}
	
}
