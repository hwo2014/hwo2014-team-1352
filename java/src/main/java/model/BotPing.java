package model;

import noobbot.SendMsg;

public class BotPing extends SendMsg {
    @Override
    public String msgType() {
        return "ping";
    }

	@Override
	public String toString() {
		return "BotPing";
	}
    
    
}