package model;

import noobbot.SendMsg;


public class BotTurbo extends SendMsg {
    public final String data;

    public BotTurbo(final String data) {
        this.data = data;
    }

    @Override
    protected String msgType() {
        return "turbo";
    }

	@Override
	public String toString() {
		return "BotTurbo [data=" +data+ "]";
	}
    
}