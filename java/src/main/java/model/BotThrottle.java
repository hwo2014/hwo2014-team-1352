package model;

import noobbot.SendMsg;

public class BotThrottle extends SendMsg {
    private double value;

    public BotThrottle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }

	@Override
	public String toString() {
		return "BotThrottle [value=" + value + "]";
	}
    
}