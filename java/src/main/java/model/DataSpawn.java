package model;

public class DataSpawn{
	public String name;
	public String color;
	public int gameTick = -1;
	public String gameId = "";	
	@Override
	public String toString() {
		return "DataCrash [name=" + name + ", color=" + color
				+ ", gameTick=" + gameTick + "]";
	}

    protected String msgType() {
        return "spawn";
    }
}
