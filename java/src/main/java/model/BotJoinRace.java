package model;

import noobbot.SendMsg;
import model.ModelBotId;

public class BotJoinRace extends SendMsg {
    public final ModelBotId botId;
    public final String trackName;
    public final String password;
    public final int carCount;  

    public BotJoinRace(ModelBotId botId, String trackName, String password,
			int carCount) 
    {
		this.botId = botId;
		this.trackName = trackName;
		this.password = password;
		this.carCount = carCount;
	}
    
	@Override
    protected String msgType() {
        return "joinRace";
    }

	@Override
	public String toString() {
		return "BotJoinRace [botId=" + botId + ", trackName=" + trackName + ", password=" + password +", carCount=" + carCount+"]";
	}
    
}