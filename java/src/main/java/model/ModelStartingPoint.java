package model;

public class ModelStartingPoint {
	public double angle;
	public Position position;
	class Position{
		public double x;
		public double y;
		@Override
		public String toString() {
			return "Position [x=" + x + ", y=" + y + "]";
		}
	}
	@Override
	public String toString() {
		return "ModelStartingPoint [angle=" + angle + ", position=" + position
				+ "]";
	}
	

}
