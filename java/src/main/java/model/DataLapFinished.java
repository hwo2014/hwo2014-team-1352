package model;

public class DataLapFinished {
	public DataYourCar car;
	public ModelLap lapTime;
	public ModelRaceTime raceTime;
	public Rank ranking;
	public String gameId="";
	public int gameTick=-1;
	
	class Rank {
		public int overall;
		public int fastestLap;
		@Override
		public String toString() {
			return "Rank [overall=" + overall + ", fastestLap=" + fastestLap
					+ "]";
		}		
	}

	@Override
	public String toString() {
		return "DataLapFinished [car=" + car + ", lapTime=" + lapTime
				+ ", raceTime=" + raceTime + ", ranking=" + ranking
				+ ", gameId=" + gameId + ", gameTick=" + gameTick + "]";
	}

    protected String msgType() {
        return "lapFinished";
    }
}
