package model;

public class DataCrash {
	public String name;
	public String color;
	public String Crashmessage = "";
	public String gameId = "";
	public int gameTick = -1;
	
	@Override
	public String toString() {
		return "DataCrash [name=" + name + ", color=" + color
				+ ", Crashmessage=" + Crashmessage + ", gameId=" + gameId
				+ ", gameTick=" + gameTick + "]";
	}
	
    protected String msgType() {
        return "crash";
    }
}
