package model;

import java.util.Arrays;

public class DataGameEnd {
	public ModelResultWithCarInfo[] results;
	public ModelResultWithCarInfo[] bestlaps;
	@Override
	public String toString() {
		return "DataGameEnd [results=" + Arrays.toString(results) + "bestlaps=" + Arrays.toString(bestlaps) +"]";
	}

    protected String msgType() {
        return "gameEnd";
    }

}
