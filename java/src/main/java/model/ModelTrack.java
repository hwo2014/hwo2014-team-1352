package model;

import java.util.Arrays;

public class ModelTrack {
	public String id;
	public String name;
	public ModelPiece[] pieces;
	public ModelLane[] lanes;
	public ModelStartingPoint startingPoint;
	
	@Override
	public String toString() {
		return "ModelTrack [id=" + id + ", name=" + name + ", pieces="
				+ Arrays.toString(pieces) + ", lanes=" + Arrays.toString(lanes)
				+ ", startingPoint=" + startingPoint + "]";
	}
	
}
