package model;

public class ModelRaceTime {
	public int laps;
	public int ticks;
	public int millis;
	
	@Override
	public String toString() {
		return "racetime [laps=" + laps + ", ticks=" + ticks + ", millis="
				+ millis + "]";
	}
	
	
	
}
