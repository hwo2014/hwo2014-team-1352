package model;

public class DataYourCar {
	public String name;
	public String color;
	
	@Override
	public String toString() {
		return "DataYourCar [name=" + name + ", color=" + color + "]";
	}
	
    protected String msgType() {
        return "yourCar";
    }

	
}
