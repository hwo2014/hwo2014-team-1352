package model;

import com.google.gson.annotations.SerializedName;

public class ModelPiece{
	
	//Remember to instead of switch to switchFlag 
	@SerializedName("switch")
	public boolean switchFlag=false;
	public double length=0;
	public int radius=0;
	public double angle=0;
	
	@Override
	public String toString() {
		return "Piece [switchFlag=" + switchFlag + ", length=" + length
				+ ", radius=" + radius + ", angle=" + angle + "]";
	}
}