package model;

import noobbot.SendMsg;


public class BotJoin extends SendMsg {
    public final String name;
    public final String key;

    public BotJoin(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }

	@Override
	public String toString() {
		return "BotJoin [name=" + name + ", key=" + key + "]";
	}
    
}
