package model;

public class ModelLane {
	public int distanceFromCenter;
	public int index;
	
	@Override
	public String toString() {
		return "ModelLane [distanceFromCenter=" + distanceFromCenter
				+ ", index=" + index + "]";
	}
	
}
