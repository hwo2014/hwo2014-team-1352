package model;
import java.util.Arrays;
public class DataCarPosition {
	public DataYourCar id;
	public double angle;
	public PiecePosition piecePosition;
	public String gameId = "";
	public int gameTick = -1;
	
	public String toString() {
		return "DataCarPosition [id=" + id + ", angle=" + angle
				+ ", piecePosition=" + piecePosition + ", gameId=" + gameId + ", gameTick=" + gameTick + "]";
	}
    protected String msgType() {
        return "carPositions";
    }
			public class PiecePosition{
			public int pieceIndex;
			public double inPieceDistance;
			public int lap;
			public Lane lane;
		
			@Override
			public String toString() {
			return "PiecePosition [pieceIndex=" + pieceIndex
					+ ", inPieceDistance=" + inPieceDistance + ", lap=" + lap
					+ ", lane=" + lane + "]";
			}
		}
	
		public class Lane{
			public int startLaneIndex;
			public int endLaneIndex;
		
			@Override
			public String toString() {
				return "Lane [startLaneIndex=" + startLaneIndex + ", endLaneIndex="
					+ endLaneIndex + "]";
			}
		}
	


}
