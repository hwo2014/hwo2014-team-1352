package model;

public class DataDNF {
	public DataYourCar car;
	public String reason;
	public String gameId="";
	public int gameTick=-1;
	@Override
	public String toString() {
		return "DataDNF [car=" + car + ", reason=" + reason + ", gameId="
				+ gameId + ", gameTick=" + gameTick + "]";
	}
	

    protected String msgType() {
        return "dnf";
    }

}
