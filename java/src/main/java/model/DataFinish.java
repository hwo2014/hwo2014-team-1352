package model;

public class DataFinish {
	public String name;
	public String color;
	public String gameId = "";
	
	@Override
	public String toString() {
		return "DataFinish [name=" + name + ", color=" + color + ", gameId="
				+ gameId + "]";
	}
	

    protected String msgType() {
        return "finish";
    }
	
}
