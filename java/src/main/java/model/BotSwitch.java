package model;

import noobbot.SendMsg;

public class BotSwitch extends SendMsg {
	private String value=null;
	
	public BotSwitch(String value) {
	        this.value = value;
	    }

	@Override
	protected Object msgData() {
	        return value;
	    }
	
	@Override
	protected String msgType() {
		// TODO Auto-generated method stub
		return "switchLane";
	}

	@Override
	public String toString() {
		return "BotSwitch [value=" + value + "]";
	}
	

}
