package model;

public class ModelRaceSession {
	public int laps;
	public int maxLapTimeMs;
	public boolean quickRace;
	
	@Override
	public String toString() {
		return "ModelRaceSession [laps=" + laps + ", maxLapTimeMs="
				+ maxLapTimeMs + ", quickRace=" + quickRace + "]";
	}
	
}
