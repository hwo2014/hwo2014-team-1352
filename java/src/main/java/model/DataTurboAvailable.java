package model;

public class DataTurboAvailable {
	public double turboDurationMilliseconds;
	public double turboDurationTicks;
	public double turboFactor;
	
	@Override
	public String toString() {
		return "DataTurboAvailable [turboDurationMilliseconds=" + turboDurationMilliseconds + 
				", turboDurationTicks=" + turboDurationTicks + ", turboFactor=" + 
				turboFactor +"]";
	}

    protected String msgType() {
        return "turboAvailable";
    }
}
