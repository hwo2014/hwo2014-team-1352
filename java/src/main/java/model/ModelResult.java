package model;

public class ModelResult {
	public int laps;
	public int ticks;
	public int millis;
	
	@Override
	public String toString() {
		return "ModelResult [laps=" + laps + ", ticks=" + ticks + ", millis="
				+ millis + "]";
	}
	
	

}
