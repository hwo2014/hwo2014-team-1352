package model;

public class ModelCar {
	public DataYourCar id;
	public Dimensions dimensions;
	
	@Override
	public String toString() {
		return "ModelCar [id=" + id + ", dimensions=" + dimensions + "]";
	}

	class Dimensions{
		public double length;
		public double width;
		public double guideFlagPosition;
		
		@Override
		public String toString() {
			return "Dimensions [length=" + length + ", width=" + width
					+ ", guideFlagPosition=" + guideFlagPosition + "]";
		}
	}
}
